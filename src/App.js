import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Start, Game, End, NotFound } from './pages'
import { GlobalProvider } from './context/Context'

function App() {

  return (
    <div className="App">
      <GlobalProvider>
        <Router>
          <Switch>
            <Route path="/" exact component={ Start }/>
            <Route path="/new" component={Game } />
            <Route path="/end" component={End} />
            <Route path="*" exact component={NotFound} />
            <Redirect from="*" to="/404" />
          </Switch>
        </Router>
      </GlobalProvider>
    </div>
  );
}

export default App;
