import React from 'react'
import ErrorImage from '../assets/error.svg'
import {Container, Row, Col, Button } from 'react-bootstrap';

const NotFound = () => {
  return (
    <Container fluid="md" style={{display: "flex", flexDirection: "column", minHeight: "100vh", justifyContent: "center", alignItems: "center"}}>
      <Row>
        <Col style={{textAlign: "center"}}>
          <h1>Not Found!</h1>
          <img src={ErrorImage} alt="How'd you get here?" />
        </Col>
      </Row>
      <Row style={{marginTop: "2rem"}}>
        <Col>
          <Button href="/" variant="info">To start a new game, go back to start screen here </Button>
        </Col>
      </Row>
    </Container>
  )
}

export default NotFound
