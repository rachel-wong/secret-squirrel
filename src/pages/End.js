import React, { useState, useEffect, useContext} from 'react'
import { Container, Row, Col, Button, Jumbotron } from 'react-bootstrap';
import { Context } from '../context/Context'

const End = () => {

  const { games } = useContext(Context)
  const [currentTotal, setCurrentTotal] = useState(0)
  const currentGameID = sessionStorage.getItem("gameID")

  useEffect(() => {
    // retrieve the object from games array with the matching ID
    // find the currentTotal from all the scores inside the plays property
    // forget the current game ID
    if (games && currentGameID) {
      let currentGame = games.filter(item => item.id === currentGameID).pop()

      if (currentGame && currentGame.plays.length > 0) {
        let currentGameTotal = currentGame.plays.map(item => item.score).reduce((a, b) => a + b)
        setCurrentTotal(currentGameTotal)
      }
      sessionStorage.clear()
    }
  }, [games, currentGameID])

  return (
    <Jumbotron fluid="md" className="start-wrapper">
      <Container>
        <Row>
          <Col className="start-text">
            <div className="line">
              <span className="char">G</span>
              <span className="char">a</span>
              <span className="char">m</span>
              <span className="char">e</span>
            </div>
            <div className="line">
              <span className="char">O</span>
              <span className="char">v</span>
              <span className="char">e</span>
              <span className="char">r</span>
            </div>
          </Col>
        </Row>
        <Row className="end-score">
          <Col>
            <h2 style={{ fontSize: "4rem" }}>Your score is: { currentTotal }</h2>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button variant="info" size="lg" href="/">Start another new game</Button>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  )
}

export default End