export { default as End } from './End'
export { default as Game } from './Game'
export { default as Start } from './Start'
export { default as NotFound } from './NotFound'