import React, { useContext } from 'react'
import { Container, Row, Col, Button, Jumbotron } from 'react-bootstrap';
import { Context } from '../context/Context'


const Start = () => {
  const { addGame } = useContext(Context)

  // create a new game object
  // persist the id of the game to add new plays to it
  function startGame() {
    const newGameObject = {
      id: Math.floor(Math.random() * 1000000),
      plays: []
    }
    addGame(newGameObject)
    sessionStorage.setItem('gameID', newGameObject.id)
  }

  return (
    <Jumbotron fluid="md" className="start-wrapper">
      <Container>
        <Row>
          <Col className="start-text">
            <div className="line">
              <span className="char">S</span>
              <span className="char">e</span>
              <span className="char">c</span>
              <span className="char">r</span>
              <span className="char">e</span>
              <span className="char">t</span>
            </div>
            <div className="line">
              <span className="char">S</span>
              <span className="char">q</span>
              <span className="char">u</span>
              <span className="char">i</span>
              <span className="char">r</span>
              <span className="char">r</span>
              <span className="char">e</span>
              <span className="char">l</span>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button variant="info" size="lg" href="/new" onClick={ () => startGame() }>Start New Game</Button>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  )
}

export default Start