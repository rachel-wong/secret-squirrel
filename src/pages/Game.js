import React, { useContext, useState, useEffect } from 'react'
import {Container, Row, Col, Button, Card, ListGroup, Form, ListGroupItem, Jumbotron } from 'react-bootstrap';
import { Context } from '../context/Context'
import axios from 'axios'
import rawDictionaryText from '../assets/words.txt'
import rawScoreValues from '../assets/scorekey.json'

const Game = () => {

  const { games, addPlay, clearHistory } = useContext(Context)
  const [hand, setHand] = useState([])
  const [wordTry, setWordTry] = useState("")
  const [dictionary, setDictionary] = useState([])
  const [scoreKey, setScoreKey] = useState({})

  const currentGameId = sessionStorage.getItem('gameID')

  /* Using mock object due to fetch
    and axios not able to retrieve
    the contents via /assets/scorekey.json */
  const mockScoreKey = {
  "a": 1,
  "b": 3,
  "c": 3,
  "d": 2,
  "e": 1,
  "f": 4,
  "g": 2,
  "h": 4,
  "i": 1,
  "j": 8,
  "k": 5,
  "l": 1,
  "m": 3,
  "n": 1,
  "o": 1,
  "p": 3,
  "q": 10,
  "r": 1,
  "s": 1,
  "t": 1,
  "u": 1,
  "v": 4,
  "w": 4,
  "x": 8,
  "y": 4,
  "z": 10
}

  /**
   * Generates random 7 alphabets
   * @method
   */

  function newHand() {
    let fullLength = 7
    let newHand = []
    let allChars = "abcdefghijklmnopqrstuvwxyz".toUpperCase().split("")
    for (let i = 0; i < fullLength; i++) {
      newHand.push(allChars[Math.floor(Math.random() * 26)])
    }
    setHand(newHand)
    sessionStorage.setItem("hand", newHand)
  }

  /**
   * Retrieves dictionary and scorekey values from api
   * Initiates once on first page load
   */
  const getDictionaryValues = async () => {
    try {

      // Promise chaining did not work here
      // let [charScoreValues, dictionary] = await Promise.allSettled([
      //   fetch('../assets/scorekey.json').then(res => console.log(res)),
      //   fetch('../assets/words.txt').then(res => res.text()).then(res => res.split("\n"))
      // ])

      let dictionaryRes = await fetch(rawDictionaryText).then(res => res.text()).then(res => res.split("\n"))
      // let scoreKeyRes = await fetch(rawScoreValues).then(res => console.log(res))

      if (dictionaryRes.length !== 0) {
        setDictionary(dictionaryRes)
        setScoreKey(mockScoreKey)
      }
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    getDictionaryValues()
  }, [])

  /**
   * Calculates the total score of a string using the scorekey object
   * @method
   * @returns total score as an integer
   */
  function calculateScore() {
    return wordTry.split("").map(item => item.toLowerCase()).reduce((accumulator, currentValue) => { return scoreKey[currentValue] + accumulator }, 0)
  }

  /**
   *
   * Sets out events for calculating scores, refreshing the UI
   * @method
   *
   */
  function wordSubmit(e) {
    e.preventDefault()

    // if the submitted word is from the deck + in dictionary
    // calculate score
    // create new play object
    // call addPlay with new play object
    // reset wordTry to empty
    // remove chars from hand
    let wordMatchesHand = hand.every(item => wordTry.includes(item))
    if (wordMatchesHand && dictionary.includes(wordTry)) {
      const newPlay = {
        "word": wordTry,
        "score": calculateScore()
      }
      addPlay(currentGameId, newPlay)
      let updatedHand = hand.filter(char => !wordTry.split("").includes(char))
      setHand(updatedHand)
    }
    // if the submitted word is from the deck + NOT in dictionary
    // reset wordTry to empty
    // trigger alert
    else if (wordMatchesHand && !dictionary.includes(wordTry)) {
    }
    // if submitted word is NOT from deck
    // reset wordTRy to empty
    // trigger different alert
    else if (!wordMatchesHand) {
    }
    setWordTry("") // common to all three scenarios
  }

  /**
   * Removes all objects in the games state
   */
  function clearAllHistory() {
    clearHistory()
  }

  return (
    <Jumbotron className="game-wrapper">
      <Container fluid="md">
        <Row>
          <h4 style={{ textAlign: "center", fontSize: "4rem", marginBottom: "1rem" }}>New Game</h4>
        </Row>
        <Row>
          <Col sm={8}>
            <Card className="game-card">
              <Card.Header><strong>Your word</strong></Card.Header>
              <Card.Body>
                <Card.Text>Hello your word</Card.Text>
              </Card.Body>
            </Card>
            <Form className="text-input" onSubmit={ (e) => wordSubmit(e) }>
              <Form.Group>
                <Form.Control type="text" value={ wordTry } onChange={(e) => setWordTry(e.target.value)} placeholder="Enter words created from your hand of letters" />
              </Form.Group>
              <Button variant="primary" type="submit">Submit</Button>
            </Form>
            <Card bg="light" className="game-card">
              <Card.Header><strong>Your hand</strong></Card.Header>
              <Card.Body>
                <div className="line">
                  {hand.length !== 0 ? (
                    hand.map((char, idx) => (<span key={ idx } className="char">{ char }</span>))
                  )
                  : ( <span>No letters left</span> )}
                </div>
              </Card.Body>
            </Card>
            <div className="game-actions">
              <Button className="game-actions__btn" variant="outline-success" size="md" onClick={ () => newHand() }>New Hand</Button>
              <Button className="game-actions__btn" variant="outline-danger" size="md" href="/end">End Current Game</Button>
              <Button className="game-actions__btn" variant="outline-warning" size="md" onClick={ () => clearAllHistory() }>Clear History</Button>
              <Button className="game-actions__btn" variant="outline-secondary" disabled>Best Hand*</Button>
            </div>
          </Col>

          <Col sm={4}>
            <Card bg="info" className="game-card game-score-card">
              <Card.Header><strong>Current Game Score</strong></Card.Header>
              <ListGroup style={{color: "black"}}className="list-group-flush">
                <ListGroupItem>240 for apple</ListGroupItem>
                <ListGroupItem>10 for he</ListGroupItem>
                <ListGroupItem><h3>Total score: 100</h3></ListGroupItem>
              </ListGroup>
            </Card>

            <Card bg="warning" className="game-card game-history-card">
              <Card.Header><strong>Game History</strong></Card.Header>
              <ListGroup className="list-group-flush">
                {games.length !== 0 ? (
                  games.map((game, idx) => (
                    <ListGroupItem key={idx}>{game.plays.map(item => item.score).reduce((a, b) => a + b, 0)}</ListGroupItem>
                  ))
                ): (
                  <ListGroupItem>No games yet</ListGroupItem>
                )}
                </ListGroup>
            </Card>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  )
}

export default Game