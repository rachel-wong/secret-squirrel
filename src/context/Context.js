import React, { createContext, useReducer } from 'react'
import AppReducer from './AppReducer'

const mockInitialState = {
  "games": [
    {
      "id": 1,
      "plays": [
        { "word": "apple", "score" : 1000},
        { "word": "orange", "score" : 2000},
        { "word": "pear", "score" : 300},
        { "word": "axes", "score" : 900}
      ]
    },
    {
      "id": 2,
      "plays": [
        { "word": "pure", "score" : 400},
        { "word": "fruit", "score" : 700},
        { "word": "money", "score" : 1000}
      ]
    },
    {
      "id": 3,
      "plays": [
        { "word": "he", "score" : 20},
        { "word": "okay", "score" : 1000}
      ]
    },
    {
      "id": 4,
      "plays": [
        { "word": "bell", "score" : 920}
      ]
    }
  ]
}

export const Context = createContext(mockInitialState)

// creates a globalprovider which is wrapped around the entire app at the highest possible level
export const GlobalProvider = ({ children }) => {

  const [state, dispatch] = useReducer(AppReducer, mockInitialState) // hardcode it for now

  // creates an id
  // creates an array of plays objects
  function addGame(gameObject) {
    console.log("addGame", gameObject)
    dispatch({
      type: 'ADD_GAME',
      payload: gameObject
    })
  }

  function addCurrentPlay(id, play) {
    dispatch({
      type: 'ADD_CURRENT_PLAY',
      payload: id, play
    })
  }

  function clearHistory() {
    dispatch({
      type: 'CLEAR_HISTORY',
      payload: null
    })
  }

  return (
    <Context.Provider value={{ games: state.games, addGame, clearHistory, addCurrentPlay }}>
      { children }
    </Context.Provider>
  )
}