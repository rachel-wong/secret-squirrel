// specifies how the global state is changed depending on the action flag
export default (state, action) => {
  switch (action.type) {
    case 'ADD_GAME':
      return {
          ...state,
          games: [action.payload, ...state.games]
      }
    case 'CLEAR_HISTORY':
      return {
        games: []
      }
    default:
      return state;
  }
}
