## :name_badge: Secret Squirrel (aka Scrabble)

> Exercise to duplicate as much of the scrabble game as possible. And also to try Bootstrap, Framer Motion and Context API

Deployed Live: [https://kind-mclean-6ac648.netlify.app/](https://kind-mclean-6ac648.netlify.app/)

:pushpin: **Dependencies**

* phosphor icon sets
* bootstrap framework and components
* json server for fake rest api
* react-router-dom
* framer motion

### :blue_book: Notes

* Not sure how to set up a button that can pass different props to the same next screen

* Fake restapi set up using `json-server` - two steps. 1) create dummy data in db.json, 2) add a new script to package.json to watch the db.json using `json-server`, 3) in terminal run `npm run server` which will run the server in the background concurrently 4) get the endpoint at `http://localhost:5000/whateverjsonname`.

```json
  scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "server": "json-server --watch db.json --port 5000" <-- this one
  },
```

The endpoint I used for this project http://localhost:5000/games

* not sure how to use an axios fetch to populate the initial state for the global context. Does the initial state always be an empty object in the `Context.js` ?

### :telescope: Extended Goals

* Set up two modes. Mode 1 (normal): score when word created from deck matches the dictionary text. Mode 2 (reverse): score when word is NOT created from any of the deck and matches the dictionary text (as per Samy Levy's suggestion)

* drag and drop functionality

* calculate the highest scoring word from the available number of letters that matches what is in the dictionary